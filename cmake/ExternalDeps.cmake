include(FetchContent)
set(FETCHCONTENT_BASE_DIR "${CMAKE_SOURCE_DIR}/external")

# GoogleTest for unit testing
FetchContent_Declare(googletest

	GIT_REPOSITORY https://github.com/google/googletest.git
	GIT_TAG release-1.11.0
)
