#pragma once

#include <string_view>
#include <variant>

#include "gtest/gtest.h"
#include "jsonpp/parser.h"
#include "utils.h"

#define NUM(arr) (sizeof(arr) / sizeof(arr[0]))

using namespace jsonpp;
using namespace testing;

TEST(parser, parse_object)
{
	auto tokenizer = Tokenizer<>("{}");
	ASSERT_TRUE(detail::parse_object(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("{  }");
	ASSERT_TRUE(detail::parse_object(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("null");
	ASSERT_FALSE(detail::parse_object(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("{\"a\": {}}");
	ASSERT_TRUE(detail::parse_object(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("{\"a\": {}, \"b\": {}}");
	ASSERT_TRUE(detail::parse_object(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("{\"a\": {}, \"b\": {},}");
	ASSERT_FALSE(detail::parse_object(tokenizer, NullVisitor{}));
}

TEST(parser, parse_array)
{
	auto tokenizer = Tokenizer<>("[]");
	ASSERT_TRUE(detail::parse_array(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("[   ]");
	ASSERT_TRUE(detail::parse_array(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("[[]]");
	ASSERT_TRUE(detail::parse_array(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("[[], []]");
	ASSERT_TRUE(detail::parse_array(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("[[],]");
	ASSERT_FALSE(detail::parse_array(tokenizer, NullVisitor{}));
}

TEST(parser, parse_string)
{
	auto tokenizer = Tokenizer<>("\"foo\"");
	ASSERT_TRUE(detail::parse_string(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("\"bar\"");
	ASSERT_TRUE(detail::parse_string(tokenizer, NullVisitor{}));

	constexpr char const* false_strings[] = {"'foo'", "\"bar", "baz"};
	for (unsigned i = 0; i < NUM(false_strings); ++i)
	{
		tokenizer = Tokenizer<>(false_strings[i]);
		ASSERT_FALSE(detail::parse_string(tokenizer, NullVisitor{}));
	}

	class StringVisitor : public FailVisitor
	{
	public:
		constexpr StringVisitor(char const* test_)
		    : test{test_}
		{}
		void string(char const* begin, std::size_t len) const
		{
			ASSERT_EQ(std::string_view(begin, len), std::string_view(test));
		}

	protected:
		char const* test;
	};

	tokenizer = Tokenizer<>("\"jsonpp\"");
	detail::parse_string(tokenizer, StringVisitor("jsonpp"));

	tokenizer = Tokenizer<>("\"Hello, \\\"world\\\"!\\n\", []");
	detail::parse_string(tokenizer, StringVisitor("Hello, \\\"world\\\"!\\n"));
}

TEST(parser, parse_number)
{
	constexpr char const* pass_tests[] = {"1024",    "-1024",   "0",      "3.14",   "-3.14",      "0.14",
	                                      "-0.14",   "2e10",    "2E10",   "2e+10",  "2E+10",      "2e-10",
	                                      "2E-10",   "0e2",     "0e0",    "0E3",    "0e-4",       "0E+6",
	                                      "1.23e+6", "1.54E-9", "0.23e6", "0x1000", "0xdeadbeef", "0b1000101"};
	constexpr char const* fail_tests[] = {"0200", "00", ".14", "33.", "00.14", "+-0", "3e", "9E-", "0.4e+", "056274"};

	for (unsigned i = 0; i < NUM(pass_tests); ++i)
	{
		auto tokenizer = Tokenizer<>(pass_tests[i]);
		ASSERT_TRUE(detail::parse_number(tokenizer, NullVisitor{})) << "   Input: '" << pass_tests[i] << "'";
	}

	for (unsigned i = 0; i < NUM(fail_tests); ++i)
	{
		auto tokenizer = Tokenizer<>(fail_tests[i]);
		ASSERT_FALSE(detail::parse_number(tokenizer, NullVisitor{})) << "   Input: '" << fail_tests[i] << "'";
	}

	class NumberVisitor : public FailVisitor
	{
		using VariantT = std::variant<std::int64_t, std::uint64_t, double, std::string_view>;

	public:
		constexpr NumberVisitor(VariantT test_)
		    : test{std::move(test_)}
		{}

		void number(std::int64_t value)
		{
			ASSERT_TRUE(std::holds_alternative<std::int64_t>(test));
			ASSERT_EQ(value, std::get<std::int64_t>(test));
		}

		void number(std::uint64_t value)
		{
			ASSERT_TRUE(std::holds_alternative<std::uint64_t>(test));
			ASSERT_EQ(value, std::get<std::uint64_t>(test));
		}

		void number(double value)
		{
			ASSERT_TRUE(std::holds_alternative<double>(test));
			ASSERT_EQ(value, std::get<double>(test));
		}

		void number(char const* begin, std::size_t len)
		{
			ASSERT_TRUE(std::holds_alternative<std::string_view>(test));
			ASSERT_EQ(std::string_view(begin, len), std::get<std::string_view>(test));
		}

	protected:
		VariantT test;
	};

	auto tokenizer = Tokenizer<>("1024");
	detail::parse_number(tokenizer, NumberVisitor(std::uint64_t(1024)));

	tokenizer = Tokenizer<>("-1024");
	detail::parse_number(tokenizer, NumberVisitor(std::int64_t(-1024)));

	tokenizer = Tokenizer<>("0");
	detail::parse_number(tokenizer, NumberVisitor(std::uint64_t(0)));

	tokenizer = Tokenizer<>("0.5");
	detail::parse_number(tokenizer, NumberVisitor(0.5));

	tokenizer = Tokenizer<>("0.5e+1");
	detail::parse_number(tokenizer, NumberVisitor(0.5e+1));

	tokenizer = Tokenizer<>("-2.5e-1");
	detail::parse_number(tokenizer, NumberVisitor(-2.5e-1));

	tokenizer = Tokenizer<>("73786976294838206464");
	detail::parse_number(tokenizer, NumberVisitor("73786976294838206464"));
}

TEST(parser, parse_keyword)
{
	auto tokenizer = Tokenizer<>("null");
	ASSERT_TRUE(detail::parse_keyword(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("true");
	ASSERT_TRUE(detail::parse_keyword(tokenizer, NullVisitor{}));

	tokenizer = Tokenizer<>("false");
	ASSERT_TRUE(detail::parse_keyword(tokenizer, NullVisitor{}));

	constexpr char const* false_keywords[] = {"none", "True", "False", "None", "empty"};
	for (unsigned i = 0; i < NUM(false_keywords); ++i)
	{
		tokenizer = Tokenizer<>(false_keywords[i]);
		ASSERT_FALSE(detail::parse_keyword(tokenizer, NullVisitor{}));
	}

	class KeywordVisitor : FailVisitor
	{
		using VariantT = std::variant<nullptr_t, bool>;

	public:
		constexpr KeywordVisitor(VariantT test_)
		    : test{std::move(test_)}
		{}
		void boolean(bool value)
		{
			ASSERT_TRUE(std::holds_alternative<bool>(test));
			ASSERT_EQ(value, std::get<bool>(test));
		}

		void null() { ASSERT_TRUE(std::holds_alternative<nullptr_t>(test)); }

	protected:
		VariantT test;
	};

	tokenizer = Tokenizer<>("true");
	detail::parse_keyword(tokenizer, KeywordVisitor(true));

	tokenizer = Tokenizer<>("false");
	detail::parse_keyword(tokenizer, KeywordVisitor(false));

	tokenizer = Tokenizer<>("null");
	detail::parse_keyword(tokenizer, KeywordVisitor(nullptr));
}

TEST(parser, parse_value)
{
	constexpr char const* pass_tests[] = {
	    "{}", " [] ", "\"0xdeadbeef\"", "  1024", "  -3.14", "2.75e-6", " null", " true", "false  ",
	};
	constexpr char const* fail_tests[] = {"{]", "{\"foo: null}", ", {}"};

	for (unsigned i = 0; i < NUM(pass_tests); ++i)
	{
		auto tokenizer = Tokenizer<>(pass_tests[i]);
		ASSERT_TRUE(detail::parse_value(tokenizer, NullVisitor{})) << "   Input: '" << pass_tests[i] << "'";
	}

	for (unsigned i = 0; i < NUM(fail_tests); ++i)
	{
		auto tokenizer = Tokenizer<>(fail_tests[i]);
		ASSERT_FALSE(detail::parse_value(tokenizer, NullVisitor{})) << "   Input: '" << fail_tests[i] << "'";
	}

	auto tokenizer = Tokenizer<>(" {\"addr\": 4096, \"types\": [\"input\", \"read-only\"], \"input\": null}   ");
	ASSERT_TRUE(detail::parse_value(tokenizer, NullVisitor{}));
	ASSERT_TRUE(tokenizer.check(TokenType::end));

	tokenizer = Tokenizer<>("   [true, false, true], {\"a\": null}");
	ASSERT_TRUE(detail::parse_value(tokenizer, NullVisitor{}));
	ASSERT_FALSE(tokenizer.check(TokenType::end));

	tokenizer = Tokenizer<>("  [true, false,]");
	ASSERT_FALSE(detail::parse_value(tokenizer, NullVisitor{}));
}

TEST(parser, parse_document)
{
	ASSERT_TRUE(parse_document("{\"foo\": [{\"bar\": 2}, {\"bar\": null}], \"enable\": false, \"id\": 2302341}",
	                           NullVisitor{}));

	ASSERT_TRUE(parse_document(
	    "{\"glossary\":{\"title\":\"example "
	    "glossary\",\"GlossDiv\":{\"title\":\"S\",\"GlossList\":{\"GlossEntry\":{\"ID\":\"SGML\",\"SortAs\":\"SGML\","
	    "\"GlossTerm\":\"Standard Generalized Markup Language\",\"Acronym\":\"SGML\",\"Abbrev\":\"ISO "
	    "8879:1986\",\"GlossDef\":{\"para\":\"A meta-markup language, used to create markup languages such as "
	    "DocBook.\",\"GlossSeeAlso\":[\"GML\",\"XML\"]},\"GlossSee\":\"markup\"}}}}}",
	    NullVisitor{}));

	ASSERT_TRUE(
	    parse_document("{\"widget\":{\"debug\":\"on\",\"window\":{\"title\":\"Sample Konfabulator "
	                   "Widget\",\"name\":\"main_window\",\"width\":500,\"height\":500},\"image\":{\"src\":\"Images\\/"
	                   "Sun.png\",\"name\":\"sun1\",\"hOffset\":250,\"vOffset\":250,\"alignment\":\"center\"},\"text\":"
	                   "{\"data\":\"Click "
	                   "Here\",\"size\":36,\"style\":\"bold\",\"name\":\"text1\",\"hOffset\":250,\"vOffset\":100,"
	                   "\"alignment\":\"center\",\"onMouseUp\":\"sun1.opacity = (sun1.opacity \\/ 100) * 90;\"}}}",
	                   NullVisitor{}));
	ASSERT_FALSE(parse_document("{\"name\": \"Joe\", \"age\": null, }", NullVisitor{}));
}

TEST(parser, compile_time_parse)
{
	constexpr bool res0 = parse_document(
	    "{\"glossary\":{\"title\":\"example "
	    "glossary\",\"GlossDiv\":{\"title\":\"S\",\"GlossList\":{\"GlossEntry\":{\"ID\":\"SGML\",\"SortAs\":\"SGML\","
	    "\"GlossTerm\":\"Standard Generalized Markup Language\",\"Acronym\":\"SGML\",\"Abbrev\":\"ISO "
	    "8879:1986\",\"GlossDef\":{\"para\":\"A meta-markup language, used to create markup languages such as "
	    "DocBook.\",\"GlossSeeAlso\":[\"GML\",\"XML\"]},\"GlossSee\":\"markup\"}}}}}",
	    NullVisitor{});
	static_assert(res0, "Expected true, actual false");

	constexpr bool res1 = parse_document("{\"name\": \"Joe\", \"age\": null, }", NullVisitor{});
	static_assert(!res1, "Expected true, actual false");

	SUCCEED();
}

TEST(parser, count_elements)
{
	CounterVisitor counter;
	parse_document("{\"foo\": [{\"bar\": 2}, {\"bar\": null}], \"enable\": false, \"id\": 2302341}", counter);
	ASSERT_EQ(counter.num_objects, 4);
	ASSERT_EQ(counter.num_arrays, 1);
	ASSERT_EQ(counter.num_strings, 0);
	ASSERT_EQ(counter.num_numbers, 2);
	ASSERT_EQ(counter.num_booleans, 1);
}
