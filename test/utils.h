#pragma once

#include <cstddef>
#include <cstdint>

#include "gtest/gtest.h"
#include "jsonpp/tokenizer.h"

using namespace jsonpp;

class BaseVisitor
{
public:
	constexpr void begin_object() const {}
	constexpr void end_object() const {}
	constexpr void begin_object_field(char const*, std::size_t) const {}
	constexpr void end_object_field() const {}
	constexpr void begin_array() const {}
	constexpr void end_array() const {}
	constexpr void begin_array_item(std::uint64_t) const {}
	constexpr void end_array_item() const {}
	constexpr void string(char const*, std::size_t) const {}
	constexpr void number(std::int64_t) const {}
	constexpr void number(std::uint64_t) const {}
	constexpr void number(double) const {}
	constexpr void number(char const*, std::size_t) const {}
	constexpr void boolean(bool) const {}
	constexpr void null() const {}
	constexpr void error(Tokenizer<char> const&) const {}
};

namespace testing
{
using NullVisitor = BaseVisitor;

class CounterVisitor : public BaseVisitor
{
public:
	std::uint32_t num_objects;
	std::uint32_t num_arrays;
	std::uint32_t num_strings;
	std::uint32_t num_numbers;
	std::uint32_t num_booleans;

	constexpr CounterVisitor()
	    : num_objects{0}
	    , num_arrays{0}
	    , num_strings{0}
	    , num_numbers{0}
	    , num_booleans{0}
	{}
	constexpr void begin_object() { num_objects++; }
	constexpr void begin_array() { num_arrays++; }
	constexpr void string(char const*, std::size_t) { num_strings++; }
	constexpr void number(std::int64_t) { num_numbers++; }
	constexpr void number(std::uint64_t) { num_numbers++; }
	constexpr void number(double) { num_numbers++; }
	constexpr void number(char const*, std::size_t) { num_numbers++; }
	constexpr void boolean(bool) { num_booleans++; }
	constexpr void null() { num_objects++; }
};

class FailVisitor
{
public:
	void begin_object() const { FAIL(); }
	void end_object() const { FAIL(); }
	void begin_object_field(char const*, std::size_t) const { FAIL(); }
	void end_object_field() const { FAIL(); }
	void begin_array() const { FAIL(); }
	void end_array() const { FAIL(); }
	void begin_array_item(std::uint64_t) const { FAIL(); }
	void end_array_item() const { FAIL(); }
	void string(char const*, std::size_t) const { FAIL(); }
	void number(std::int64_t) const { FAIL(); }
	void number(std::uint64_t) const { FAIL(); }
	void number(double) const { FAIL(); }
	void number(char const*, std::size_t) const { FAIL(); }
	void boolean(bool) const { FAIL(); }
	void null() const { FAIL(); }
	void error(Tokenizer<char> const&) const { FAIL(); }
};
}  // namespace testing
