#pragma once

#include "gtest/gtest.h"
#include "jsonpp/tokenizer.h"

using namespace jsonpp;

/* Test tokenizing an empty string. */
TEST(tokenizer, tokenize_empty)
{
	Tokenizer<> tokenizer("");
	ASSERT_EQ(tokenizer->type, TokenType::end);
	ASSERT_EQ(tokenizer->get_len(), 0);
}

/* Test tokenizing a word. */
TEST(tokenizer, tokenize_words)
{
	auto tokenizer = Tokenizer<>("jsonpp");
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "jsonpp");

	tokenizer = Tokenizer<>("c99");
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "c99");

	tokenizer = Tokenizer<>("jsonpp_parse");
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "jsonpp_parse");

	tokenizer = Tokenizer<>("__magic__");
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "__magic__");

	tokenizer = Tokenizer<>("jsonpp-parse");
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "jsonpp");
	++tokenizer;
	ASSERT_NE(tokenizer->type, TokenType::word);
	++tokenizer;
	ASSERT_EQ(tokenizer->type, TokenType::word);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "parse");

	tokenizer = Tokenizer<>("4ever");
	ASSERT_NE(tokenizer->type, TokenType::word);
	++tokenizer;
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "ever");
}

/* Test tokenizing a sequence of digits. */
TEST(tokenizer, tokenize_digit)
{
	auto tokenizer = Tokenizer<>("123");
	ASSERT_EQ(tokenizer->type, TokenType::digit);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "123");

	tokenizer = Tokenizer<>("3.14");
	ASSERT_EQ(tokenizer->type, TokenType::digit);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "3");
	++tokenizer;
	ASSERT_NE(tokenizer->type, TokenType::digit);
	++tokenizer;
	ASSERT_EQ(tokenizer->type, TokenType::digit);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "14");

	tokenizer = Tokenizer<>("user123");
	ASSERT_NE(tokenizer->type, TokenType::digit);
	++tokenizer;
	ASSERT_EQ(tokenizer->type, TokenType::end);
}

/* Test tokenizing whitespace. */
TEST(tokenizer, tokenize_whitespace)
{
	auto tokenizer = Tokenizer<>("    ");
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "    ");

	tokenizer = Tokenizer<>("\t  ");
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "\t  ");

	tokenizer = Tokenizer<>("\n\n");
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "\n\n");

	tokenizer = Tokenizer<>("");
	ASSERT_NE(tokenizer->type, TokenType::whitespace);

	tokenizer = Tokenizer<>(" jsonpp ");
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
	++tokenizer;
	ASSERT_NE(tokenizer->type, TokenType::whitespace);
	++tokenizer;
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
}

/* Test tokenizing strings. */
TEST(tokenizer, tokenize_string)
{
	auto tokenizer = Tokenizer<>("\"jsonpp\"");
	ASSERT_EQ(tokenizer->type, TokenType::string);
	ASSERT_EQ(std::string_view(tokenizer->begin, tokenizer->get_len()), "\"jsonpp\"");

	tokenizer = Tokenizer<>("'jsonpp'");
	ASSERT_NE(tokenizer->type, TokenType::string);

	tokenizer = Tokenizer<>("\"jsonpp");
	ASSERT_NE(tokenizer->type, TokenType::string);
}

/* Test tokenizing various tokens. */
TEST(tokenizer, tokenize_misc)
{
	auto tokenizer = Tokenizer<>(",:+-[]{}");
	ASSERT_EQ(tokenizer->type, TokenType::comma);
	ASSERT_EQ((++tokenizer)->type, TokenType::colon);
	ASSERT_EQ((++tokenizer)->type, TokenType::plus);
	ASSERT_EQ((++tokenizer)->type, TokenType::minus);
	ASSERT_EQ((++tokenizer)->type, TokenType::lbracket);
	ASSERT_EQ((++tokenizer)->type, TokenType::rbracket);
	ASSERT_EQ((++tokenizer)->type, TokenType::lbrace);
	ASSERT_EQ((++tokenizer)->type, TokenType::rbrace);
}

/* Test the function to check the type of the generated token. */
TEST(tokenizer, check)
{
	auto tokenizer = Tokenizer<>("foo: 123");
	ASSERT_TRUE(tokenizer.check(TokenType::word));
	ASSERT_TRUE(tokenizer.check(TokenType::word, TokenType::digit, TokenType::whitespace));
	ASSERT_TRUE(tokenizer.check(TokenType::digit, TokenType::word));
	ASSERT_FALSE(tokenizer.check(TokenType::digit));
	ASSERT_FALSE(tokenizer.check(TokenType::string, TokenType::digit));
	tokenizer++;
	ASSERT_TRUE(tokenizer.check(TokenType::colon));
	ASSERT_TRUE(tokenizer.check(TokenType::comma, TokenType::colon, TokenType::plus));
	ASSERT_FALSE(tokenizer.check(TokenType::comma, TokenType::minus, TokenType::plus));
	tokenizer++;
	ASSERT_TRUE(tokenizer.check(TokenType::whitespace));
	ASSERT_TRUE(tokenizer.check(TokenType::comma, TokenType::colon, TokenType::whitespace));
	ASSERT_FALSE(tokenizer.check(TokenType::string, TokenType::word, TokenType::plus));
	tokenizer++;
	ASSERT_TRUE(tokenizer.check(TokenType::digit));
	ASSERT_TRUE(tokenizer.check(TokenType::word, TokenType::digit, TokenType::whitespace));
	ASSERT_FALSE(tokenizer.check(TokenType::string, TokenType::word, TokenType::plus));
}

/* Test the function to expect the type of the generated token. */
TEST(tokenizer, expect)
{
	auto tokenizer = Tokenizer<>("foo: 123");
	ASSERT_TRUE(tokenizer.expect(TokenType::word));
	ASSERT_TRUE(tokenizer.expect(TokenType::colon));
	ASSERT_TRUE(tokenizer.expect(TokenType::whitespace));
	ASSERT_TRUE(tokenizer.expect(TokenType::digit));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));

	tokenizer = Tokenizer<>("[null,\"foo\"]");
	ASSERT_TRUE(tokenizer.expect(TokenType::lbracket));
	ASSERT_TRUE(tokenizer.expect(TokenType::word));
	ASSERT_TRUE(tokenizer.expect(TokenType::comma));
	ASSERT_TRUE(tokenizer.expect(TokenType::string));
	ASSERT_TRUE(tokenizer.expect(TokenType::rbracket));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));
	ASSERT_TRUE(tokenizer.expect(TokenType::end));
}

/* Test the function(s) to skip token(s). */
TEST(tokenizer, skip)
{
	auto tokenizer = Tokenizer<>("\tfoo: 123");
	tokenizer.skip(TokenType::whitespace);
	ASSERT_EQ(tokenizer->type, TokenType::word);
	tokenizer.skip(TokenType::word);
	ASSERT_EQ(tokenizer->type, TokenType::colon);
	tokenizer.skip(TokenType::word);
	ASSERT_EQ(tokenizer->type, TokenType::colon);
	tokenizer.skip(TokenType::colon, TokenType::whitespace);
	ASSERT_EQ(tokenizer->type, TokenType::whitespace);
	tokenizer.skip(TokenType::colon, TokenType::whitespace);
	ASSERT_EQ(tokenizer->type, TokenType::digit);

	tokenizer = Tokenizer<>("\t::foo,,\n");
	tokenizer.skip_all(TokenType::whitespace, TokenType::colon, TokenType::comma);
	ASSERT_EQ(tokenizer->type, TokenType::word);
	tokenizer.skip_all(TokenType::whitespace, TokenType::colon, TokenType::comma);
	ASSERT_EQ(tokenizer->type, TokenType::word);
	++tokenizer;
	tokenizer.skip_all(TokenType::whitespace, TokenType::colon, TokenType::comma);
	ASSERT_EQ(tokenizer->type, TokenType::end);
}
