#pragma once

#include "jsonpp/tokenizer.h"

namespace jsonpp
{
/**
 * @brief Try to parse an input string as a JSON document.
 *
 * @tparam VisitorT the class of the visitor object
 * @param text the input string to parse
 * @param visitor the object that receives the callbacks
 * @return true if the input is valid JSON, false otherwise
 */
template<typename VisitorT>
constexpr bool parse_document(char const* text, VisitorT&& visitor);
}  // namespace jsonpp

#include "parser.inl"
