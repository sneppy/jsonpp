#include <cerrno>

#include "jsonpp/misc.h"

namespace jsonpp
{
namespace detail
{
/* Check that the substring matches the given keyword. */
template<std::size_t klen>
static constexpr bool check_keyword(char const* str, std::size_t len, char const (&keyword)[klen])
{
	if (len != klen - 1)
	{
		// Length must match
		return false;
	}

	for (std::size_t i = 0; i < len; ++i)
	{
		if (str[i] != keyword[i])
		{
			return false;
		}
	}

	return true;
}

/* Check that the current token is a valid exponent (i.e. 'e' or 'E').

   Does not generate the next token. */
template<typename CharT>
static constexpr bool check_exponent(Tokenizer<CharT> const& tokenizer)
{
	return tokenizer->get_len() == 1 && (tokenizer->begin[0] == 'e' || tokenizer->begin[0] == 'E');
}

template<typename CharT>
static constexpr bool parse_integral(char const* text, std::uint64_t& value_out)
{
	std::uint64_t value = std::strtoull(text, nullptr, 10);
	if (errno == ERANGE)
	{
		return false;
	}

	value_out = value;
	return true;
}

/* Parse prefix as an integral value. */
template<typename CharT>
static constexpr bool parse_integral(CharT const* prefix, std::int64_t& value_out)
{
	auto value = std::strtoll(prefix, nullptr, 10);
	if (errno == ERANGE)
	{
		return false;
	}

	value_out = value;
	return true;
}

template<typename CharT>
static constexpr bool parse_integral(CharT const* prefix, std::uint64_t& value_out)
{
	auto value = std::strtoull(prefix, nullptr, 10);
	if (errno == ERANGE)
	{
		return false;
	}

	value_out = value;
	return true;
}

/* Parse prefix as a floating point value. */
template<typename CharT>
static constexpr bool parse_floating(CharT const* prefix, double& value_out)
{
	auto value = std::strtod(prefix, nullptr);
	if (errno == ERANGE)
	{
		return false;
	}

	value_out = value;
	return true;
}

/* Parse a JSON value (object, array, string, number or keyword).

   Skips leading and trailing whitespace. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_value(Tokenizer<CharT>&, VisitorT&&);

/* Parse a JSON object. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_object(Tokenizer<CharT>&, VisitorT&&);

/* Parse a field of a JSON object. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_object_field(Tokenizer<CharT>&, VisitorT&&);

/* Parse a JSON array. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_array(Tokenizer<CharT>&, VisitorT&&);

/* Parse the item of a JSON array. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_array_item(Tokenizer<CharT>&, VisitorT&&, std::size_t);

/* Parse a string. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_string(Tokenizer<CharT>&, VisitorT&&);

/* Parse a number. */
template<typename CharT, typename VisitorT>
static constexpr bool parse_number(Tokenizer<CharT>&, VisitorT&&);

/* Parse a JSON keyword (e.g. `null`, `true` or `false`). */
template<typename CharT, typename VisitorT>
static constexpr bool parse_keyword(Tokenizer<CharT>&, VisitorT&&);

template<typename CharT, typename VisitorT>
constexpr bool parse_value(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	checkpoint.skip_all(TokenType::whitespace);
	if (parse_object(checkpoint, FORWARD(visitor)))
		;
	else if (parse_array(checkpoint, FORWARD(visitor)))
		;
	else if (parse_string(checkpoint, FORWARD(visitor)))
		;
	else if (parse_number(checkpoint, FORWARD(visitor)))
		;
	else if (parse_keyword(checkpoint, FORWARD(visitor)))
		;
	else
	{
		// TODO: Visitor error
		return false;
	}

	checkpoint.skip_all(TokenType::whitespace);
	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_object(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	auto tokenizer = checkpoint;
	if (!tokenizer.expect(TokenType::lbrace))
	{
		return false;
	}

	visitor.begin_object();

	if (parse_object_field(tokenizer, FORWARD(visitor)))
	{
		while (tokenizer.expect(TokenType::comma))
		{
			if (!parse_object_field(tokenizer, FORWARD(visitor)))
			{
				// TODO: Visitor error
				return false;
			}
		}
	}

	tokenizer.skip_all(TokenType::whitespace);
	if (!tokenizer.expect(TokenType::rbrace))
	{
		visitor.error(tokenizer);
		return false;
	}

	visitor.end_object();

	tokenizer.skip_all(TokenType::whitespace);
	checkpoint = tokenizer;
	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_object_field(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	auto tokenizer = checkpoint;
	tokenizer.skip_all(TokenType::whitespace);
	if (!tokenizer.check(TokenType::string))
	{
		return false;
	}

	visitor.begin_object_field(tokenizer->begin, tokenizer->get_len());
	++tokenizer;

	tokenizer.skip_all(TokenType::whitespace);
	if (!tokenizer.expect(TokenType::colon))
	{
		return false;
	}

	if (!parse_value(tokenizer, FORWARD(visitor)))
	{
		visitor.error(tokenizer);
		return false;
	}

	visitor.end_object_field();

	checkpoint = tokenizer;
	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_array(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	auto tokenizer = checkpoint;
	if (!tokenizer.expect(TokenType::lbracket))
	{
		return false;
	}

	visitor.begin_array();

	std::size_t idx = 0;
	if (parse_array_item(tokenizer, FORWARD(visitor), idx))
	{
		while (tokenizer.expect(TokenType::comma))
		{
			if (!parse_array_item(tokenizer, FORWARD(visitor), ++idx))
			{
				visitor.error(tokenizer);
				return false;
			}
		}
	}

	tokenizer.skip_all(TokenType::whitespace);
	if (!tokenizer.expect(TokenType::rbracket))
	{
		visitor.error(tokenizer);
		return false;
	}

	visitor.end_array();

	checkpoint = tokenizer;
	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_array_item(Tokenizer<CharT>& checkpoint, VisitorT&& visitor, std::uint64_t idx)
{
	visitor.begin_array_item(idx);

	if (!parse_value(checkpoint, FORWARD(visitor)))
	{
		visitor.error(checkpoint);
		return false;
	}

	visitor.end_array_item();

	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_string(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	if (checkpoint.check(TokenType::string))
	{
		// Strip quotes
		visitor.string(checkpoint->begin + 1, checkpoint->get_len() - 2);

		++checkpoint;
		return true;
	}

	// Not a string
	return false;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_number(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	auto tokenizer    = checkpoint;
	auto const* begin = tokenizer->begin;

	// Sign of the number
	bool sign = tokenizer.expect(TokenType::minus);

	if (tokenizer->begin[0] == '0' && (tokenizer->get_len() != 1))
	{
		// Leading zero must not be followed by other digits
		visitor.error(tokenizer);
		return false;
	}
	else if (!tokenizer.check(TokenType::digit))
	{
		return false;
	}

	++tokenizer;
	if (tokenizer.check(TokenType::full_stop) || check_exponent(tokenizer))
	{
		if (tokenizer.expect(TokenType::full_stop) && !tokenizer.expect(TokenType::digit))
		{
			visitor.error(tokenizer);
			return false;
		}

		if (check_exponent(tokenizer))
		{
			(++tokenizer).skip(TokenType::plus, TokenType::minus);
			if (!tokenizer.expect(TokenType::digit))
			{
				visitor.error(tokenizer);
				return false;
			}
		}

		double value = .0;
		if (parse_floating(begin, value))
		{
			visitor.number(value);
		}
		else
		{
			// Fall back to string callback
			visitor.number(begin, tokenizer->begin - begin);
		}
	}
	else if (sign)
	{
		// Parse signed integer
		std::int64_t value = 0;
		if (parse_integral(begin, value))
		{
			visitor.number(value);
		}
		else
		{
			// Fall back to string callback
			visitor.number(begin, tokenizer->begin - begin);
		}
	}
	else
	{
		// Parse unsigned integer
		std::uint64_t value = 0;
		if (parse_integral(begin, value))
		{
			visitor.number(value);
		}
		else
		{
			// Fall back to string callback
			visitor.number(begin, tokenizer->begin - begin);
		}
	}

	checkpoint = tokenizer;
	return true;
}

template<typename CharT, typename VisitorT>
constexpr bool parse_keyword(Tokenizer<CharT>& checkpoint, VisitorT&& visitor)
{
	if (check_keyword(checkpoint->begin, checkpoint->get_len(), "null"))
	{
		visitor.null();
	}
	else if (check_keyword(checkpoint->begin, checkpoint->get_len(), "true"))
	{
		visitor.boolean(true);
	}
	else if (check_keyword(checkpoint->begin, checkpoint->get_len(), "false"))
	{
		visitor.boolean(false);
	}
	else
	{
		// Not a keyword
		return false;
	}

	++checkpoint;
	return true;
}
}  // namespace detail

template<typename VisitorT>
constexpr bool parse_document(char const* text, VisitorT&& visitor)
{
	Tokenizer<char> tokenizer(text);
	return detail::parse_value(tokenizer, FORWARD(visitor)) && tokenizer.check(TokenType::end);
}
}  // namespace jsonpp
