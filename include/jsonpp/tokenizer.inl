#define RANGEI(x, a, b) ((x) >= a && (x) <= b)
#define ALPHA_UPPER(x) RANGEI(x, 'A', 'Z')
#define ALPHA_LOWER(x) RANGEI(x, 'a', 'z')
#define ALPHA(x) (ALPHA_LOWER(x) || ALPHA_UPPER(x) || x == '_')
#define DIGIT(x) RANGEI(x, '0', '9')
#define WHITESPACE(x) ((x) == ' ' || (x) == '\t' || (x) == '\n')

namespace jsonpp
{
template<typename CharT>
constexpr Tokenizer<CharT>::Tokenizer(CharT const* stream_)
    : tok{}
    , stream{stream_}
    , lineno{0}
    , col{0}
{
	// Generate the first token
	next();
}

template<typename CharT>
constexpr void Tokenizer<CharT>::next()
{
	// Reset current token
	tok.type  = TokenType::none;
	tok.begin = stream;
	tok.end   = tok.begin + 1;

	switch (stream[0])
	{
		case '\0':
			// Don't parse NULL character
			tok.type = TokenType::end;
			tok.end  = tok.begin;
			break;

		case ',':
			tok.type = TokenType::comma;
			break;

		case ':':
			tok.type = TokenType::colon;
			break;

		case '.':
			tok.type = TokenType::full_stop;
			break;

		case '+':
			tok.type = TokenType::plus;
			break;

		case '-':
			tok.type = TokenType::minus;
			break;

		case '{':
			tok.type = TokenType::lbrace;
			break;

		case '}':
			tok.type = TokenType::rbrace;
			break;

		case '[':
			tok.type = TokenType::lbracket;
			break;

		case ']':
			tok.type = TokenType::rbracket;
			break;

		case '"':
		{
			auto del = stream[0];
			tok.type = TokenType::string;
			for (++tok.end; tok.end[-1] != del || tok.end[-2] == '\\'; ++tok.end)
			{
				if (tok.end[0] == '\0')
				{
					// Unterminated string
					tok.type = TokenType::none;
					tok.end  = tok.begin;
					break;
				}
			}
		}
		break;

		default:
		{
			if (ALPHA(stream[0]))
			{
				tok.type = TokenType::word;
				for (; ALPHA(*tok.end) || DIGIT(*tok.end); ++tok.end)
					;
			}
			else if (DIGIT(stream[0]))
			{
				tok.type = TokenType::digit;
				for (; DIGIT(*tok.end); ++tok.end)
					;
			}
			else if (WHITESPACE(stream[0]))
			{
				tok.type = TokenType::whitespace;
				for (; WHITESPACE(*tok.end); ++tok.end)
					;
			}
		}
		break;
	}

	// Advance stream
	stream = tok.end;
}
}  // namespace jsonpp

#undef WHITESPACE
#undef DIGIT
#undef ALPHA
#undef ALPHA_LOWER
#undef ALPHA_UPPER
#undef RANGEI
