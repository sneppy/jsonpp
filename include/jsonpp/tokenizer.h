#pragma once

#include <cstdint>

namespace jsonpp
{
/**
 * @brief Enumeration defining the JSON token types.
 */
enum class TokenType : std::uint8_t
{
	begin,
	end,
	word,
	digit,
	whitespace,
	string,
	comma,
	colon,
	full_stop,
	plus,
	minus,
	lbracket,
	rbracket,
	lbrace,
	rbrace,
	none
};

/**
 * @brief Represents a JSON token.
 *
 * @tparam CharT the character class
 */
template<typename CharT>
struct Token
{
	/* The type of the token. */
	TokenType type;

	/* Pointer to the beginning of the token. */
	CharT const* begin;

	/* Pointer to the end of the token. */
	CharT const* end;

	/**
	 * @brief Get the length of the token in number of characters.
	 */
	constexpr std::size_t get_len() const noexcept { return end - begin; }
};

/**
 * @brief Class used to tokenize a JSON input stream.
 *
 * @tparam CharT the character class
 */
template<typename CharT = char>
class Tokenizer
{
	using TokenT = Token<CharT>;

public:
	/**
	 * @brief Create a new JSON tokenizer to tokenize the given input stream.
	 *
	 * @param[in] stream_ the input stream to tokenize
	 */
	constexpr Tokenizer(CharT const* stream_);

	/**
	 * @brief Get a reference to the generated token.
	 */
	constexpr TokenT const& operator*() const noexcept { return tok; }

	/**
	 * @brief Get a pointer to the generate token.
	 */
	constexpr TokenT const* operator->() const noexcept { return &*(*this); }

	/**
	 * @brief Generate the next token.
	 *
	 * @return ref to self
	 */
	constexpr Tokenizer& operator++()
	{
		next();
		return *this;
	}

	/**
	 * @brief Generate the next token.
	 *
	 * @return copy of self before generating the next token
	 */
	constexpr Tokenizer operator++(int)
	{
		auto other = *this;
		++*this;
		return other;
	}

	/**
	 * @brief Check that the generated token has the given type(s).
	 *
	 * @param type the type to check against
	 * @param types more types to check against
	 * @return true if the generated token matches any of the given types
	 */
	constexpr bool check(TokenType type, auto... types) const noexcept
	{
		return tok.type == type || ((tok.type == types) || ...);
	}

	/**
	 * @brief Like check() but generate the next token if the type matches.
	 *
	 * @param type the type to check against
	 * @param types more types to check against
	 * @return true if the generated token matches any of the given types
	 */
	constexpr bool expect(TokenType type, auto... types)
	{
		if (check(type, types...))
		{
			next();
			return true;
		}

		return false;
	}

	/**
	 * @brief Like expect(), but ignore the return value.
	 *
	 * Use this in place of expect to make it clear that we don't care about
	 * the check.
	 *
	 * @param type the type to check against
	 * @param types more types to check against
	 */
	constexpr void skip(TokenType type, auto... types) { expect(type, types...); }

	/**
	 * @brief Like skip(), but skip all next tokens that pass the check.
	 *
	 * @param type the type to check against
	 * @param types more types to check against
	 */
	constexpr void skip_all(TokenType type, auto... types)
	{
		while (check(type, types...))
		{
			next();
		}
	}

protected:
	/* The last generated token. */
	TokenT tok;

	/* Pointer to the current position in the input stream. */
	CharT const* stream;

	/* Current line number. */
	int lineno;

	/* Current column number. */
	int col;

	/* Generate the next token. */
	constexpr void next();

private:
	Tokenizer() = delete;
};
}  // namespace jsonpp

#include "tokenizer.inl"
