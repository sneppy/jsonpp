#!/bin/bash

# Run default pre-commit hook, if available
GIT_DIR=$(git rev-parse --git-dir)
if [ -f "$GIT_DIR/hooks/pre-commit.sample" ]; then
	./${GIT_DIR}/hooks/pre-commit.sample
fi

# Get list of staged source files
CHANGED_FILES=$(git diff --name-only --cached -- *.cpp *.h *.inl)
if [ -z "${CHANGED_FILES}" ]; then
	# No staged file, nothing to do
	exit 0
fi

# Run clang-format and check for errors
# Check clang-format version is at least 10
semver-ge() {
	[ "$2" = "$(echo -e "$1\n$2" | sort -V | head -n1)" ]
}

CLANG_FORMAT_VERSION=$(clang-format --version | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+" | head -n1)
if ! semver-ge ${CLANG_FORMAT_VERSION} 10.0.0; then
	>&2 echo "Found clang-format version '$CLANG_FORMAT_VERSION', but 10.0.0 is the minimum required"
	echo "Skipping clang-format checks ..."
else
	# Run clang-format on staged files
	if ! clang-format --dry-run -Werror ${CHANGED_FILES}; then
		echo "Formatting issues found, applying automatic fix"
		clang-format -i ${CHANGED_FILES}
		echo "Review the changes and \`commit --amend\` when satisfied"
	fi
fi
